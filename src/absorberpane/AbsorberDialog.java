package absorberpane;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.geometry.HPos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Absorber;
import model.ApplicationClasses;
import sun.nio.cs.ext.TIS_620;

/**
 *
 * @author Pascal Frigge
 */
class AbsorberDialog extends Stage {

    private Absorber absorber;

    private TextField nameTextField;
    private Spinner<Double> xSpinner;
    private Spinner<Double> ySpinner;
    private ComboBox<ApplicationClasses> classComboBox;

    public AbsorberDialog() {
        this(new Absorber());
    }

    public AbsorberDialog(Absorber absorber) {
        if (absorber == null) {
            throw new IllegalArgumentException();
        }

        Scene scene = new Scene(this.getUI());
        this.setScene(scene);
        this.initModality(Modality.APPLICATION_MODAL);

        this.setAbsorber(absorber);
    }

    private Parent getUI() {
        AnchorPane anchorPane = new AnchorPane();

        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(5);
        int rowIndex = 0;
        Label label;
        // Bezeichnung
        this.nameTextField = new TextField();
        label = new Label("Bezeichnung:");
        GridPane.setHalignment(label, HPos.RIGHT);
        gridPane.addRow(rowIndex, label, this.nameTextField);
        rowIndex++;
        // X
        this.xSpinner = new Spinner<>(0, Double.MAX_VALUE, 0, 1);
        this.xSpinner.setEditable(true);
        label = new Label("X:");
        GridPane.setHalignment(label, HPos.RIGHT);
        gridPane.addRow(rowIndex, label, this.xSpinner);
        rowIndex++;
        // Y
        this.ySpinner = new Spinner<>(0, Double.MAX_VALUE, 0, 1);
        this.ySpinner.setEditable(true);
        label = new Label("Y:");
        GridPane.setHalignment(label, HPos.RIGHT);
        gridPane.addRow(rowIndex, label, this.ySpinner);
        rowIndex++;
        // ApplicationClass
        this.classComboBox = new ComboBox<>();
        this.classComboBox.setItems(FXCollections.observableArrayList(ApplicationClasses.A1_Sprache, ApplicationClasses.A2_Musik));
        label = new Label("Klasse:");
        GridPane.setHalignment(label, HPos.RIGHT);
        gridPane.addRow(rowIndex, label, this.classComboBox);
        rowIndex++;
        // Summe
        Label summenLabel = new Label();
        this.xSpinner.valueProperty().addListener(observable -> {
            summenLabel.setText(this.xSpinner.getValue() + this.ySpinner.getValue() + "");
        });
        this.ySpinner.valueProperty().addListener(observable -> {
            summenLabel.setText(this.xSpinner.getValue() + this.ySpinner.getValue() + "");
        });
        label = new Label("Summe:");
        GridPane.setHalignment(label, HPos.RIGHT);
        gridPane.addRow(rowIndex, label, summenLabel);
        rowIndex++;
        
        anchorPane.getChildren().add(gridPane);
        AnchorPane.setTopAnchor(gridPane, 10d);
        AnchorPane.setLeftAnchor(gridPane, 10d);
        AnchorPane.setRightAnchor(gridPane, 10d);
        AnchorPane.setBottomAnchor(gridPane, 50d); // prevent overlap with buttons

        // Buttons
        Button cancelButton = new Button("Abbrechen");
        cancelButton.setCancelButton(true);
        cancelButton.setOnAction(actionEvent -> {
            this.absorber = null;
            this.close();
        });
        Button saveButton = new Button("Speichern");
        saveButton.setDefaultButton(true);
        saveButton.setOnAction(actionEvent -> {
            this.absorber.setName(this.nameTextField.getText());
            this.absorber.setX(this.xSpinner.getValue());
            this.absorber.setY(this.ySpinner.getValue());
            this.absorber.setApplicationClass(this.classComboBox.getValue());
            this.close();
        });
        HBox buttonHBox = new HBox(5, saveButton, cancelButton);
        anchorPane.getChildren().add(buttonHBox);
        AnchorPane.setBottomAnchor(buttonHBox, 10d);
        AnchorPane.setRightAnchor(buttonHBox, 10d);

        return anchorPane;
    }

    public Absorber getAbsorber() {
        return this.absorber;
    }

    private void setAbsorber(Absorber absorber) {
        this.absorber = absorber;

        this.nameTextField.setText(this.absorber.getName());
        this.nameTextField.selectAll();
        this.xSpinner.getValueFactory().setValue(this.absorber.getX());
        this.ySpinner.getValueFactory().setValue(this.absorber.getY());
        this.classComboBox.setValue(this.absorber.getApplicationClass());
    }
}
