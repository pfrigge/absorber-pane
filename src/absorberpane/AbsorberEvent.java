package absorberpane;

import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

/**
 *
 * @author Pascal Frigge
 */
public class AbsorberEvent extends Event {

    public static final EventType<AbsorberEvent> ABSORBER_ANY = new EventType<>("ABSORBER_ANY");
    public static final EventType<AbsorberEvent> ABSORBER_NEW = new EventType<>(ABSORBER_ANY, "ABSORBER_NEW");
    public static final EventType<AbsorberEvent> ABSORBER_EDITED = new EventType<>(ABSORBER_ANY, "ABSORBER_EDITED");
    public static final EventType<AbsorberEvent> ABSORBER_DELETED = new EventType<>(ABSORBER_ANY, "ABSORBER_DELETED");

    public AbsorberEvent(EventType<AbsorberEvent> eventType) {
        super(eventType);
    }

    public AbsorberEvent(Object source, EventTarget target, EventType<AbsorberEvent> eventType) {
        super(source, target, eventType);
    }
}
