package absorberpane;

import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import model.Absorber;

/**
 *
 * @author Pascal Frigge
 */
public class AbsorberPane extends VBox {

    private List<Absorber> absorberList;

    public AbsorberPane() {
        this.absorberList = new ArrayList<>();
        this.buildRows();
    }

    public AbsorberPane(List<Absorber> absorberList) {
        if (absorberList == null) {
            throw new IllegalArgumentException();
        }

        this.absorberList = absorberList;
        this.buildRows();
    }
    
    public List<Absorber> getAbsorberList()
    {
        return this.absorberList;
    }

    private void buildRows() {
        this.getChildren().clear();
        for (Absorber absorber : this.absorberList) {
            this.buildRow(absorber);
        }
        this.buildAddNewRow();
    }

    private void buildRow(Absorber absorber) {
        HBox hBox = new HBox(10);
        hBox.setAlignment(Pos.BASELINE_LEFT);
        TextField nameTextField = new TextField(absorber.getName());
//        nameTextField.setEditable(false);
        nameTextField.textProperty().addListener(observable -> {
            absorber.setName(nameTextField.getText());
        });
//        nameTextField.focusedProperty().addListener(observable -> {
//            if(nameTextField.isFocused()) {
//                nameTextField.selectAll();
//            }
//        });
        hBox.getChildren().add(nameTextField);
        Label summenLabel = new Label("Summe: " + absorber.getSum());
        hBox.getChildren().add(summenLabel);
        Label klassenLabel = new Label("Klasse: " + absorber.getApplicationClass().toString());
        hBox.getChildren().add(klassenLabel);
        Button editButton = new Button("Bearbeiten");
        editButton.setOnAction(actionEvent -> {
            AbsorberDialog dialog = new AbsorberDialog(absorber);
            dialog.showAndWait();
            if(dialog.getAbsorber() != null) { // nicht abgebrochen
                this.buildRows();
                this.fireEvent(new AbsorberEvent(AbsorberEvent.ABSORBER_EDITED));
            }
        });
        hBox.getChildren().add(editButton);
        Button deleteButton = new Button("Löschen");
        deleteButton.setOnAction(actionEvent -> {
            this.absorberList.remove(absorber);
            this.buildRows();
            this.fireEvent(new AbsorberEvent(AbsorberEvent.ABSORBER_DELETED));
        });
        hBox.getChildren().add(deleteButton);

        this.getChildren().add(hBox);
    }

    private void buildAddNewRow() {
        Button newButton = new Button("Absorber hinzufügen");
        newButton.setOnAction(actionEvent -> {
            AbsorberDialog dialog = new AbsorberDialog();
            dialog.showAndWait();
            Absorber newAbsorber = dialog.getAbsorber();
            if (newAbsorber != null) {
                this.absorberList.add(newAbsorber);
                this.buildRows();
                this.fireEvent(new AbsorberEvent(AbsorberEvent.ABSORBER_NEW));
            }
        });
        this.getChildren().add(newButton);
    }
}
