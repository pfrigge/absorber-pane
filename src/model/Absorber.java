package model;

/**
 *
 * @author Pascal Frigge
 */
public class Absorber {

    private String name;
    private double x;
    private double y;
    private ApplicationClasses applicationClass;
    
    public Absorber() {
        this.applicationClass = ApplicationClasses.A1_Sprache;
    }

    public Absorber(String name) {
        this();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public ApplicationClasses getApplicationClass() {
        return applicationClass;
    }

    public void setApplicationClass(ApplicationClasses applicationClass) {
        this.applicationClass = applicationClass;
    }
    
    public double getSum() {
        return this.x + this.y;
    }
}
