package model;

/**
 *
 * @author Pascal
 */
public enum ApplicationClasses {
    A1_Sprache("Sprache"),
    A2_Musik("Musik");
    
    private String text;
    
    ApplicationClasses(String text) {
        this.text = text;
    }
    
    @Override
    public String toString() {
        return this.text;
    }
}
