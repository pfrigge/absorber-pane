package test;

import absorberpane.AbsorberEvent;
import absorberpane.AbsorberPane;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.Absorber;

/**
 *
 * @author Pascal Frigge
 */
public class Test extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Label summenLabel = new Label("Die Summe aller Absorberwerte ist 0.");
        summenLabel.setPadding(new Insets(10));
        
        AbsorberPane absorberPane = new AbsorberPane();
        absorberPane.setSpacing(10);
        absorberPane.setPadding(new Insets(10));
        absorberPane.addEventHandler(AbsorberEvent.ABSORBER_ANY, event -> {
            // Hier kann statt dem Alert die Berechnung aktualisiert werden.
            // absorberPane.getAbsorberList();
            
            int summe = 0;
            for(Absorber a : absorberPane.getAbsorberList()){
                summe += a.getSum();
            }
            summenLabel.setText("Die Summe aller Absorberwerte ist " + summe + ".");
            
            // Animation bei Aktualisierung
            DropShadow shadow = new DropShadow();
            shadow.setColor(Color.LIGHTGREEN);
            shadow.setSpread(0.75);
            summenLabel.setEffect(shadow);
            Timeline shadowAnimation = new Timeline(
                new KeyFrame(Duration.ZERO, new KeyValue(shadow.radiusProperty(), 20d)),
                new KeyFrame(Duration.seconds(0.8), new KeyValue(shadow.radiusProperty(), 0d)));
            shadowAnimation.setOnFinished(action -> { summenLabel.setEffect(null); });
            shadowAnimation.play();
        });
        
        VBox root = new VBox(20);
        root.getChildren().addAll(absorberPane, summenLabel);
        
        Scene scene = new Scene(root, 800, 500);
        
        primaryStage.setTitle("AbsorberPane-Test");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
